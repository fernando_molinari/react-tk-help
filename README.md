# react-tk-help

> reusable help package for tk

[![NPM](https://img.shields.io/npm/v/react-tk-help.svg)](https://www.npmjs.com/package/react-tk-help) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-tk-help
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-tk-help'
import 'react-tk-help/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

gpl-3.0-only © [fernando_molinari](https://github.com/fernando_molinari)
