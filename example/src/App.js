import React from 'react'

import { ExampleComponent } from 'react-tk-help'
import 'react-tk-help/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
